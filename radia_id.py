# ----------------------------------------------------------
# PyRadiaUndulators library
# radia_undulator.py
# Library of Radia models for undulators
#
# Gael Le Bec, ESRF, 2019
# ----------------------------------------------------------

# --- Imports
import radia as rad
import radia_util as rad_uti
import radia_mat_util as rad_mat
from matplotlib.pyplot import plot, show, xlabel, ylabel, figure, title
from itertools import accumulate
import pickle
import numpy as np
from datetime import datetime
from math import pi
from copy import deepcopy

# Constants
h = 4.135667516e-15 # Plank constant (eV s)
c = 299792458 # Speed of light (m/s)

# -------------------------------------------
# Parameters for undulator Radia models
# -------------------------------------------
class UndParam():
    """
    Base class for undulator parameters
    """


# -------------------------------------------
# Parameters for PPM undulator Radia models
# -------------------------------------------
class PPMUndParam(UndParam):
    """
    Radia Parameters for PPM undulator
    """
    def __init__(self, period=35, n_periods=4, gap=11, mag_width=45, mag_height=17.5, mag_chamfer=4, mag_gap=0.05,
                 ext_length_ratio=0.6, mat='ndfeb', br_h=1.22, br_v=1.3, area_max=100, long_sub=3,
                 color_h=[1, 0, 1], color_v=[0, 1, 1], thcn=0.001):
        """
       Initialize an undulator parameter object
       :param period: undulator period [mm] 
       :param n_periods: number of period 
       :param gap: undulator gap [mm] 
       :param mag_width: PM block width [mm] 
       :param mag_height: PM block heigth [mm] 
       :param mag_chamfer: PM block chamfer [mm] 
       :param mag_gap: gap between PM blocks 
       :param ext_length_ratio: extremity block length to std block length ratio 
       :param mat: material: 'ndfeb', 'sm2co17', 'smco5' or 'ferrite' 
       :param br_h: Br of H blocks 
       :param br_v: Br of V blocks 
       :param area_max: max area for mesh triangles [mm^2] 
       :param long_sub: longitudinal subdivision
       :param color_h: color for H blocks
       :param color_v: color for V blocks
       :param thcn: line thickness (DOES NOT WORK)
        """
        self.period = period
        self.n_periods = n_periods
        self.gap = gap
        self.mag_width = mag_width
        self.mag_height = mag_height
        self.mag_chamfer = mag_chamfer
        self.mag_gap = mag_gap
        self.ext_length_ratio = ext_length_ratio
        self.mat = mat
        self.br_h = br_h
        self.br_v = br_v
        self.area_max = area_max
        self.long_sub = long_sub
        self.color_h = color_h
        self.color_v = color_v
        self.thcn = thcn
        self.und_type = 'ppm'


# -------------------------------------------
# Parameters for APPLE-II undulator Radia models
# -------------------------------------------
class APPLE2UndParam(UndParam):
    def __init__(self, period=70, n_periods=4, gap=11, phase=0, offset=0, mag_width=25, mag_height=13.5, mag_chamfer=4, 
        mag_chamfer_centre=0.5, mag_slot=4, mag_gap=0.1, transverse_gap=0.5, 
        ext=[1.59/70, 10/70, 5.97/70, 8.61/70, 2.11/70, 6.18/70], mat='ndfeb', br_h=1.22, br_v=1.3, 
        area_max=100, long_sub=3, color_h=[1, 0, 1], color_v=[0, 1, 1], thcn=0.001):
        """
        Initialize an undulator parameter object
        :param period: undulator period [mm] 
        :param n_periods: number of period 
        :param gap: undulator gap [mm] 
        :param phase: phase displacement [mm]
        :param offset: offset displacement [mm]
        :param mag_width: PM block width [mm] 
        :param mag_height: PM block heigth [mm] 
        :param mag_chamfer: PM block chamfer (main) [mm] 
        :param mag_chamfer_centre: PM block chamfer on axis [mm]
        :param mag_slot: PM block slot dimension  [mm]
        :param mag_gap: longitudinal gap between PM blocks [mm]
        :param transverse_gap: transverse gap between PM blocks [mm]
        :param ext: [gap0/period, mag0/period, gap1/period, ...] extremity sequence of gaps and magnet lengths normalized to the period
        :param mat: material: 'ndfeb', 'sm2co17', 'smco5' or 'ferrite' 
        :param br_h: Br of H blocks 
        :param br_v: Br of V blocks 
        :param area_max: max area for mesh triangles [mm^2] 
        :param long_sub: longitudinal subdivision
        :param color_h: color for H blocks
        :param color_v: color for V blocks
        :param thcn: line thickness (DOES NOT WORK)
        """

        self.period = period
        self.n_periods = n_periods
        self.gap = gap
        self.phase = phase
        self.offset = offset
        self.mag_width = mag_width
        self.mag_height = mag_height
        self.mag_chamfer = mag_chamfer
        self.mag_chamfer_centre = mag_chamfer_centre
        self.mag_slot = mag_slot
        self.mag_gap = mag_gap
        self.transverse_gap = transverse_gap
        self.ext = deepcopy(ext)
        self.mat = mat
        self.br_h = br_h
        self.br_v = br_v
        self.area_max = area_max
        self.long_sub = long_sub
        self.color_h = color_h
        self.color_v = color_v
        self.thcn = thcn
        self.und_type = 'apple2'


# -------------------------------------------
# Parameters for hybrid undulator
# -------------------------------------------
class HybridUndParam(UndParam):
    """
    Radia Parameters for a hybrid undulator
    """
    def __init__(self, period, n_periods=4, gap=6, mag_width=38, mag_height=23, mag_chamfer=3, pole_width=25,
                 pole_height=18, pole_position=0.05, pole_chamfer=2, pole_mag_ratio=0.5084, mag_gap=0.05, ext=[0.85, 2.8, 0.25, 2.8, 0.45],
                 mag_mat='ndfeb', br=1.565, pole_mat='fecov', mag_area_max=30, pole_area_max=5, mag_long_sub=3,
                 pole_long_sub=5, mag_color=[0, 1, 1], pole_color=[1, 0, 1], thcn=0.001):
        """
        Parameters for hybrid undulator
        :param period: undulator period [mm]
        :param n_periods: number of periods
        :param gap: undulator gap [mm] 
        :param mag_width: PM block width [mm] 
        :param mag_height: PM block heigth [mm] 
        :param mag_chamfer: PM block chamfer [mm] 
        :param pole_width: pole width [mm] 
        :param pole_height: pole heigth [mm] 
        :param pole_position: vertical position of pole [mm]
        :param pole_chamfer: pole chamfer [mm] 
        :param pole_mag_ratio: pole-to-magnet thickness ratio
        :param mag_gap: gap between PM blocks 
        :param ext: defines the extremity [mag1_ly_ratio [1], gap1 [mm],
                pole_ly_ratio [1], gap2 [mm], mag2_ly_ratio [1]] 
        :param mag_mat: material: 'ndfeb', 'sm2co17', 'smco5' or 'ferrite' 
        :param br: Br of H blocks 
        :param pole_mat: material: 'xc6' (XC 6 / AISI 1006), 'feco' (FeCo), 'fecov' (FeCoV) 
        :param mag_area_max: max area for pm block mesh triangles [mm^2] 
        :param pole_area_max: max area for pole mesh triangles [mm^2] 
        :param mag_long_sub: longitudinal subdivision for PM blocks
        :param pole_long_sub: longitudinal subdivision for poles
        :param mag_color: color for PM blocks
        :param pole_color: color for blocks
        :param thcn: line thickness (DOES NOT WORK)
        """
        self.period = period
        self.n_periods = n_periods
        self.gap = gap
        self.mag_width = mag_width
        self.mag_height = mag_height
        self.mag_chamfer = mag_chamfer
        self.pole_width = pole_width
        self.pole_height = pole_height
        self.pole_position = pole_position
        self.pole_chamfer = pole_chamfer
        self.pole_mag_ratio = pole_mag_ratio
        self.mag_gap = mag_gap
        self.ext = deepcopy(ext)
        self.mag_mat = mag_mat
        self.br = br
        self.pole_mat = pole_mat
        self.mag_area_max = mag_area_max
        self.pole_area_max = pole_area_max
        self.mag_long_sub = mag_long_sub
        self.pole_long_sub = pole_long_sub
        self.mag_color = mag_color
        self.pole_color = pole_color
        self.thcn = thcn
        self.und_type = 'hybrid'


# -------------------------------------------
# Parameters for hybrid wigglers
# -------------------------------------------
class HybridWigParam(UndParam):
    """
    Radia parameters for hybrid wigglers
    """
    def __init__(self, period, n_periods, gap, mag_width=105, mag_height=[120, 100], mag_chamfer=10, pole_length=24,
                 pole_width=38, pole_height=90, ext_pole=[12.25,9], ext_mag=[12, 12, 10], mag_mat='ndfeb', br=1.3,
                 pole_mat='xc6', mag_area_max=300, pole_area_max=100, mag_long_sub=3, pole_long_sub=5,
                 mag_color=[[0, 1, 1],[0, 0.5, 1]], pole_color=[1, 0, 1]):
        """
        Parameters for hybrid wiggler -- ESRF ID17 type
        :param period: period (mm)
        :param n_periods: number of periods
        :param gap: gap (mm)
        :param mag_width=105: width of the magnet blocks (mm)
        :param mag_height=[120, 100]: height of the magnet blocks [main, side] (mm)
        :param mag_chamfer=10: magnet chamfer (mm)
        :param pole_length=24: length of the poles (mm)
        :param pole_width=38: width of the poles (mm)
        :param pole_height=90: height of the poles (mm)
        :param ext_pole=[1,3]: extremity pole params [pole_width (mm), distance to previous obj (mm)]
        :param ext_mag=[1,10, 10]: extremity mag params [mag_width (mm), distance to extremity pole (mm),
                                                        extremity chamfer (mm)]]
        :param mag_mat='ndfeb': magnet block material
        :param br=1.3: remanent field (T)
        :param pole_mat='xc6': pole material
        :param mag_area_max=200: max triangle area in magnet blocks
        :param pole_area_max=100: max triangle area in poles
        :param mag_long_sub=3: long. subdivision in magnets
        :param pole_long_sub=3: long. subdivision in poles
        :param mag_color=[[0,1,1],[0,0.5,1]]: color of magnet blocks [main, side]
        :param pole_color=[1,0,1]: color of poles
        """

        self.period = period
        self.n_periods = n_periods
        self.gap=gap
        self.mag_width = mag_width
        self.mag_height = mag_height
        self.mag_chamfer = mag_chamfer
        self.pole_length = pole_length
        self.pole_width = pole_width
        self.pole_height = pole_height
        self.ext_pole = deepcopy(ext_pole)
        self.ext_mag = deepcopy(ext_mag)
        self.mag_mat = mag_mat
        self.br = br
        self.pole_mat = pole_mat
        self.mag_area_max = mag_area_max
        self.pole_area_max = pole_area_max
        self.mag_long_sub = mag_long_sub
        self.pole_long_sub = pole_long_sub
        self.mag_color = mag_color
        self.pole_color = pole_color


# -------------------------------------------
# Parameters for PPM undulator Radia models
# -------------------------------------------
class PPMPhaseShifterParam(UndParam):
    """
    Radia Parameters for PPM phase shifters
    """
    def __init__(self, period, gap, dist=[0, 0], double_h=False, mag_width=45, mag_height=25, mag_chamfer=4, mag_gap=0.05,
                 ext_length_ratio=0.6, mat='ndfeb', br_h=1.22, br_v=1.3, area_max=100, long_sub=3,
                 color_h=[1, 0, 1], color_v=[0, 1, 1], thcn=0.001):
        """
       Initialize an undulator parameter object
       :param period: undulator period [mm]
       :param gap: undulator gap [mm]
       :param dist=[0, 0]: longitudinal gaps. dist[0] in the y=0 plane, dist[1] before last block [mm]
       :param double_h=False: double the H blocks if True
       :param mag_width: PM block width [mm] (default = 45 mm)
       :param mag_height: PM block heigth [mm] (default = 25 mm)
       :param mag_chamfer: PM block chamfer [mm] (default = 4 mm)
       :param mag_gap: gap between PM blocks (default = 0.05 mm)
       :param ext_length_ratio: extremity block length to std block length ratio (default=0.6)
       :param mat: material: 'ndfeb', 'sm2co17', 'smco5' or 'ferrite' (default = 'ndfeb')
       :param br_h: Br of H blocks (default = 1.22 T)
       :param br_v: Br of V blocks (default = 1.3 T)
       :param area_max: max area for mesh triangles [mm^2] (default = 100)
       :param long_sub: longitudinal subdivision
       :param color_h: color for H blocks
       :param color_v: color for V blocks
       :param thcn: line thickness (DOES NOT WORK)
        """
        self.period = period
        self.gap = gap
        self.dist = dist
        self.double_h = double_h
        self.mag_width = mag_width
        self.mag_height = mag_height
        self.mag_chamfer = mag_chamfer
        self.mag_gap = mag_gap
        self.ext_length_ratio = ext_length_ratio
        self.mat = mat
        self.br_h = br_h
        self.br_v = br_v
        self.area_max = area_max
        self.long_sub = long_sub
        self.color_h = color_h
        self.color_v = color_v
        self.thcn = thcn
        self.und_type = 'ppm_ps'


# -------------------------------------------
# Parameters for staggered undulators
# -------------------------------------------
class StaggeredUndParam(UndParam):
    """"
    Parameters for staggered undulator
    """
    def __init__(self, period, n_periods=5, gap=4, sol_field=1, pole_width=30, pole_height=20, pole_period_ratio=0.57,
                 ext=[0, 0.5], pole_mat='fecov', pole_area_max=10, pole_sub=10, pole_long_sub=3,
                 pole_color=[0, 1, 1], thcn=0.001):
        self.period = period
        self.n_periods = n_periods
        self.gap = gap
        self.sol_field = sol_field
        self.pole_width = pole_width
        self.pole_height = pole_height
        self.pole_period_ratio = pole_period_ratio
        self.pole_mat = pole_mat
        self.pole_area_max = pole_area_max
        self.pole_sub=pole_sub
        self.pole_long_sub = pole_long_sub
        self.pole_color = pole_color
        self.ext = ext
        self.und_type = 'staggered'
        self.thcn = thcn


# -------------------------------------------
# Precision parameters for Radia solver
# -------------------------------------------
class PrecParams():
    def __init__(self, tolerance=1e-5, max_iter=10000):
        """
        Initialize precision parameters for the Radia solver
        :param tolerance: tolerance (default = 1e-5)
        :param max_iter:  maximum number of iterations (default=10000)
        """
        self.tolerance = tolerance
        self.max_iter = max_iter


# -------------------------------------------
# Radia undulator class
# -------------------------------------------
class Undulator():
    """
    Radia model undulator
    """
    def set_gap(self, gap):
        """
        Build an undulator with a new gap (for PPM and hybrid planar undulators)
        :param gap: new gap
        """
        # --- Hold the new gap
        self.radia_und_param.gap = gap

        # --- Build a new undulator
        self.obj = self.build_undulator(self.sym)

        # --- Solve
        self.solve()

    def solve(self,  print_switch=True):
        """
        Radia solver
        :param  print_switch=True: quiet mode if False
        """
        # --- Get tolerance parameters
        tol = self.radia_prec_param.tolerance
        max_iter = self.radia_prec_param.max_iter
        
        # --- Solve
        self.time_start = datetime.today()
        if print_switch:
            t = self.time_start
            print(t.day, '/', t.month, '/', t.year, ' at ', t.hour, ':', t.minute, ':', t.second,
                  ' Start to solve the magnetization problem...')
        self.result = rad.Solve(self.obj, tol, max_iter)
        self.time_end = datetime.today()
        if print_switch:
            t = self.time_end
            print(t.day, '/', t.month, '/', t.year, ' at ', t.hour, ':', t.minute, ':', t.second,
                  ' Magnetization problem solved.')
        
        # --- Undulator field
        self.b_eff, self.b_1, self.b_peak = self.field_calc()

        # --- Deflection parameter
        self.k = 0.09337 * self.radia_und_param.period * self.b_eff

    def field(self, xyz_end, xyz_start=[0, 0, 0], n=100, b='bz'):
        """
        Compute the field along a straight line
        :param xyz_end: end point [x, y, z]
        :param xyz_start: starting point [x, y, z] (default = [0, 0, 0])
        :param n: number of points (default = 100)
        :param b: field component (default = 'bz')
        :return: x, y, z, d, bz: positions, distance to initial point and field
        """
        # --- Sampling
        x0, y0, z0 = xyz_start
        x1, y1, z1 = xyz_end
        # Steps
        dx = (x1 - x0) / (n - 1)
        dy = (y1 - y0) / (n - 1)
        dz = (z1 - z0) / (n - 1)
        # Positions
        x = [x0 + k * dx for k in range(n)]
        y = [y0 + k * dy for k in range(n)]
        z = [z0 + k * dz for k in range(n)]
        xyz = [[x[k], y[k], z[k]] for k in range(n)]
        # Distance to initial point
        d = [((x[k] - x[k - 1]) ** 2 + (y[k] - y[k - 1]) ** 2 + (z[k] - z[k - 1]) ** 2) ** 0.5 for k in range(1, n)]
        d = list(accumulate(d))
        d.insert(0, 0)

        # --- Field computation
        bz = rad.Fld(self.obj, b, xyz)

        # --- Return
        return x, y, z, d, bz

    def plot_field(self, xyz_end, xyz_start=[0, 0, 0], n=100, b='bz', x_axis='d', plot_show=True, plot_title=''):
        """"
        Compute and plot the field along a straight line
        :param xyz_end: end point [x, y, z]
        :param xyz_start: starting point [x, y, z] (default = [0, 0, 0])
        :param n: number of points (default = 100)
        :param b: field component (default = 'bz')
        :param xaxis: defines the x axis of the plot, x_axis = 'x', 'y', 'z' or 'd' (default = 'd', i.e. distance)
        :param show: show the plot if True
        :param plot_title: plot title
        :return: a matplotlib figure
        """

        # --- Compute the field
        x, y, z, d, bz = self.field(xyz_end=xyz_end, xyz_start=xyz_start, n=n, b=b)
        # --- Plot
        fig = figure()
        if x_axis == 'x':
            l = x
        elif x_axis == 'y':
            l = y
        elif x_axis == 'z':
            l = z
        else:
            l = d
        plot(l, bz)
        xlabel('Distance (mm)')
        ylabel('Field (T)')
        title(plot_title)
        if plot_show:
            show()
        return fig

    def traj(self, e, init_cond=[0, 0, 0, 0], y_range=None, n_points=100):
        """
        Compute the trajectory of an electron in the undulator
        :param e: energy of the electron (GeV)
        :param init_cond: initial coordinates (mm) and angles (rad) [x0, dx/dy0, z0, dz/dy0] (default: [0, 0, 0, 0])
        :param y_range: initial and final value of the longitudinal coordinate (mm)
                                    (default: +/- 3 periods before the limits of the object)
        :param n_points: number of points (default: 100)
        :return: the trajectory: n_points lists of [y, x, dxdy, z, dzdy]
        """

        # --- Limits
        if y_range is None:
            lim = rad.ObjGeoLim(self.und)[3] + 3 * self.radia_und_param.period
            y_range = [-lim, lim]

        # --- Compute the trajectory
        trj = rad.FldPtcTrj(self.obj, e, init_cond, y_range, n_points)

        # --- Transpose
        trj_np = np.array(trj)
        trj_tr = np.ndarray.tolist(trj_np.transpose())

        # --- Return the trajectory
        return trj_tr

    def plot_traj(self, e, init_cond=[0, 0, 0, 0], y_range=None, n_points=100, x_or_z='x',
                  plot_show=True, plot_title=''):
        """
        Compute and plot the trajectory of a particle in the undulator
        :param e: energy of the electron (GeV)
        :param init_cond: initial coordinates (mm) and angles (rad) [x0, dx/dy0, z0, dz/dy0] (default: [0, 0, 0, 0])
        :param y_range: initial and final value of the longitudinal coordinate (mm)
                            (default: +/- 3 periods before the limits of the object)
        :param n_points: number of points (default: 100)
        :param x_or_z: select the trajectory component to plot
                        -- 'x': horizontal position (default)
                        -- 'z': vertical position
                        -- 'dxdy': horizontal angle
                        -- 'dzdy': vertical angle
        :param plot_show: show the plot if True
        :param plot_title: title of the plot
        :return: a matplotlib figure
        """

        # --- Compute the trajectory
        y, x, dxdy, z, dzdy = self.traj(e, init_cond, y_range, n_points)
        # --- Plot
        fig = figure()
        if x_or_z == 'x':
            trj = x
            ylab = 'Horiz. position (mm)'
        elif x_or_z == 'z':
            trj = z
            ylab = 'Vert. position (mm)'
        elif x_or_z == 'dxdy':
            trj = dxdy
            ylab = 'Horiz. angle (rad)'
        elif x_or_z == 'dzdy':
            trj = dzdy
            ylab = 'Vert. angle (rad)'
        else:
            trj = y
            ylab = 'Long. position (mm)'
        plot(y, trj)
        xlabel('Long. position (mm)')
        ylabel(ylab)
        title(plot_title)
        if plot_show:
            show()
        return fig

    def field_int(self, xyz_end, xyz_start=None, n=100, b='bz', method='fld_int'):
        """
        Compute the field integral along a straight line
        :param xyz_end: end point [x, y, z]
        :param xyz_start=None: starting point [x, y, z] (-xyz_end if None)
        :param n: number of points (default = 100)
        :param b: field component (default = 'bz')
        :param method: computation method:  -- 'fld_int': use rad.FldInt
                                            -- 'fld_int_fin': use rad.FldInt('fin')
                                            -- 'fld': numerical integration of field from rad.Fld
        :return: x, y, z, d, ib, bz: positions, distance to initial point, integrated field and field
        """
        if xyz_start is None:
            xyz_start = [-xyz for xyz in xyz_end]
        if method == 'fld_int':
            ib = rad.FldInt(self.obj, 'inf', b, xyz_start, xyz_end)
            x, y, z, d, b = None, None, None, None, None
        if method == 'fld_int_fin':
            ib = rad.FldInt(self.obj, 'fin', b, xyz_start, xyz_end)
            x, y, z, d, b = None, None, None, None, None
        if method == 'fld':
            # --- Compute the field
            x, y, z, d, b = self.field(xyz_end, xyz_start=xyz_start, n=n, b=b)
            # --- Integrate
            dxyz = ( (xyz_end[0] - xyz_start[0]) ** 2
                   + (xyz_end[1] - xyz_start[1]) ** 2
                   + (xyz_end[2] - xyz_start[2]) ** 2 ) ** 0.5 / (n - 1)
            ib = dxyz * np.cumsum(b)
        # --- return
        return x, y, z, d, ib, b

    def plot_field_int(self, xyz_end, xyz_start=[0, 0, 0], dir_int=[0, 1, 0], n=100, b='bz', x_axis='d', plot_show=True, plot_title=''):
        """"
        Compute and plot the field along a straight line
        :param xyz_end: end point [x, y, z]
        :param xyz_start: starting point [x, y, z] (default = [0, 0, 0])
        :param dir_int: direction of the integration (default = [0, 1, 0]) 
        :param n: number of points (default = 100)
        :param b: field component (default = 'bz')
        :param method: computation method:  -- 'fld_int': use rad.FldInt
                                            -- 'fld_int_fin': use rad.FldInt('fin')
                                            -- 'fld': numerical integration of field from rad.Fld
        :param xaxis: defines the x axis of the plot, x_axis = 'x', 'y', 'z' or 'd' (default = 'd', i.e. distance)
        :param show: show the plot if True
        :param plot_title: plot title
        :return: a matplotlib figure
        """

        # --- Init
        dxyz = [(xyz_end[k] - xyz_start[k]) / (n - 1) for k in range(3)]
        x, y, z, d = np.zeros(n), np.zeros(n), np.zeros(n), np.zeros(n)
        ib = np.zeros(n)
        # --- Compute the field
        for k in range(n):
            x[k], y[k], z[k] = xyz_start[0] + k * dxyz[0], xyz_start[1] + k * dxyz[1], xyz_start[2] + k * dxyz[2]
            xyz_end_k = [x[k] + 1000 * dir_int[0], y[k] + 1000 * dir_int[1], z[k] + 1000 * dir_int[2]]
            xyz_start_k = [x[k] - 1000 * dir_int[0], y[k] - 1000 * dir_int[1], z[k] - 1000 * dir_int[2]]
            xk, yk, zk, dk, ib[k], bk = self.field_int(xyz_end=xyz_end_k, xyz_start=xyz_start_k, b=b)

        for k in range(n-1):
            d[k+1] = d[k]  + ((x[k+1] - x[k]) ** 2 + (y[k+1] - y[k]) ** 2 + (z[k+1] - z[k]) ** 2) ** 0.5

        # --- Plot
        fig = figure()
        if x_axis == 'x':
            l = x
        elif x_axis == 'y':
            l = y
        elif x_axis == 'z':
            l = z
        else:
            l = d
        plot(l, ib)
        xlabel('Distance (mm)')
        ylabel('Field integral (T mm)')
        title(plot_title)
        if plot_show:
            show()
        return fig

    def phase_int(self, xyz_end, xyz_start=None, n=100, b='bz'):
        """
        Compute the phase integral of the device
        :param xyz_end: end point [x, y, z]
        :param xyz_start=None: starting point [x, y, z] (-xyz_end if None)
        :param n: number of points (default = 100)
        :param b: field component (default = 'bz')
        :return: x, y, z, d, phase_int: positions, distance to initial point, phase integral (T^2mm^3)
        """

        # --- Field integral
        x, y, z, d, ib, bz = self.field_int(xyz_end, xyz_start=xyz_start, n=n, b=b, method='fld')

        # --- b^2 integral
        if xyz_start is not None:
            dxyz = ((xyz_end[0] - xyz_start[0]) ** 2
                    + (xyz_end[1] - xyz_start[1]) ** 2
                    + (xyz_end[2] - xyz_start[2]) ** 2) ** 0.5 / (n - 1)
        else:
            dxyz = ((2 * xyz_end[0]) ** 2 + (2 * xyz_end[1]) ** 2 + (2 * xyz_end[2] ) ** 2) ** 0.5 / (n - 1)
        ib2 = dxyz * np.cumsum(ib * ib)

        # --- Return
        return x, y, z, d, ib2

    def plot_geo(self):
        """
        Plot the undulator geometry
        """
        rad.ObjDrwOpenGL(self.obj)

    def save(self, filename, path=''):
        """
        Save the undulator object to filename.rad and its parameters to filename.radp.
        Use the load() method for loading the undulator.

        The filename.rad can be opened with the Mathematica interface, using the RadUtiLoad[] function

        :param filename: file name without extension
        :param path: absolute path if specified (default = '', i.e. relative path)
        """

        # --- Save the undulator
        # Dump the undulator
        dmp = rad.UtiDmp(self.obj, 'bin')
        # Write to a file
        f = open(path + filename + '.rad', 'wb')
        f.write(dmp)
        f.close()

        # --- Save the parametres
        f = open(path + filename + '.radp', 'wb')
        pickle.dump(self.radia_und_param, f)
        pickle.dump(self.radia_prec_param, f)
        f.close()

    def export(self, filename, path=''):
        """
        Save the Radia model of the undulator to filename.rad
        :param filename: file name without extension
        :param path: absolute path if specified (default = '', i.e. relative path)
        :return: True
        """
        rad_uti.save(self.obj, filename, path=path)

    def load(self, filename, path=''):
        """"
        Load the undulator to filename.rad and its parameters to filename.radp.

        The filename.und can be also opened with the Mathematica interface, using the RadUtiLoad[] function

        :param filename: file name without extension
        :param path: absolute path if specified (default = '', i.e. relative path)
        :return: True if no error, else False
        """
        # --- Load the parametres
        try:
            f = open(path + filename + 'radp', 'rb')
            self.radia_und_param = pickle.load(f)
            self.radia_prec_param = pickle.load(f)
            f.close()
        except FileNotFoundError:
            print('Error: File not found')
            return

        # --- Load the undulator
        try:
            f = open(path + filename + '.rad', 'rb')
            und_bin = f.read()
            f.close()
            self.obj = rad.UtiDmpPrs(und_bin)
        except FileNotFoundError:
            print('Error: File not found')
            return

    def force(self, normal_plane=None, point=None):
        """
        Compute the forces from one part of the object to the other part
        The object is cut in two subparts defined by a plane and a point
        :param normal_plane=None: normal vector [nx, ny, nz] ([0, 0, 1] if None)
        :param point=None: [0, 0, 0] if None
        :return: fx, fy, fz : magnetic forces (N)
        """

        # --- Initialize
        if normal_plane is None:
            normal_plane = [0, 0, 1]
        if point is None:
            point = [0, 0, 0]
        # --- Duplicate and cut
        obj_dpl = rad.ObjDpl(self.obj, 'FreeSym->True')
        obj_0, obj_1 = rad.ObjCutMag(obj_dpl, point, normal_plane)
        # --- Compute the forces
        # FORCES COMPUTATIONS NOT YET AVAILABLE WITH RADIA PYTHON !!!!
        return 0, 0, 0

    def wavelength(self, e=6, n=1, theta=0):
        """
        Return the radiation wavelength and energy
        :param e=6: energy of the electron beam (GeV)
        :param n=1: harmonic number
        :param theta: observer angle (rad)
        :return: wavelength (nm), energy (keV)
        """
        try:
            gamma = 1957 * e
            lambda_r = 0.001 * self.radia_und_param.period / (2 * n * gamma ** 2) \
                       * (1 + self.k ** 2 / 2 + theta ** 2) # (m)
            e_r = h * c / lambda_r # (eV)
            return 1e9 * lambda_r, 1e-3 * e_r
        except AttributeError:
            print('Error: Missing attribute in wavelength()')
            return None, None

    def print_wavelength(self, e=6, n=1, theta=0):
        """
        Compute and print the wavelength and energy
        :param e=6: energy of the electron beam (GeV)
        :param n=1: harmonic number
        :param theta: observer angle (rad)
        :return: None
        """

        lambda_r, e_r = self.wavelength(e=e, n=n, theta=theta)
        if lambda_r is not None:
            print('Wavelength: ', lambda_r, ' nm')
            print('Energy: ', e_r, 'keV')

    def break_symetry(self):
        """
        Remove all symetries in the radia object
        """
        obj_no_sym = rad.ObjDpl(self.obj, 'FreeSym->True')
        self.obj = obj_no_sym
        
    def field_calc(self, x0=0, z0=0, n0=32):
        """
        Compute the effective value, the first harmonic and the peak value of the field
        :param x0: transverse horizontal position
        :param z0: transverse vertical position
        :param n0: number of field points
        :return b_eff, b_1, b_peak: field values
        """
        # Compute the field
        y0 = self.radia_und_param.period
        _, _, _, _, bz = self.field([x0, y0 / 2, z0], [x0, - y0 / 2, z0], n=n0+1)

        # Fourier transform
        ft = np.fft.rfft(bz[0:-1])

        # Effective field
        b_eff = 0
        for k in range(1, n0 // 2):
            b_eff += np.abs(ft[k]) ** 2
        b_eff = 2 / n0 * b_eff ** 0.5 

        # First harmonic
        b_1 = 2 / n0 * np.abs(ft[1])

        # Peak field
        b_peak = np.ptp(bz) / 2

        return b_eff, b_1, b_peak

# -------------------------------------------
# Planar PPM undulator class
# -------------------------------------------
class PPMUndulator(Undulator):
    def __init__(self, radia_und_param, radia_prec_param=None, sym=True, solve_switch=True, print_switch=True):
        """
        Initialize a Radia undulator
        :param radia_und_param: undulator parameters
        :param radia_prec_param: precision parameters for the Radia solver
        :param sym: apply the symmetries (default = True). Build 1/8 undulator if False.
        :param solve_switch=True: solve the magnetization problem if True
        :param print_switch=True: quiet mode if False
        """
        # Check the undulator type
        if radia_und_param.und_type != 'ppm':
            print('Error: parameters are not for PPM undulator.')
            return

        # Hold the parameters
        self.radia_und_param = radia_und_param

        # Precision parameters
        if radia_prec_param is None:
            self.radia_prec_param = PrecParams()
        else:
            self.radia_prec_param = radia_prec_param

        # Build an undulator
        self.obj = self.build_undulator(sym)

        # Solve
        if solve_switch:
            self.solve(print_switch=print_switch)

    def contour_block(self):
        """
        Compute magnet contour and associated subdivision
        :return contour, subdivision paits
        """

        lx = self.radia_und_param.mag_width
        lz = self.radia_und_param.mag_height
        ch = self.radia_und_param.mag_chamfer
        contour = [[0, 0], [0, lx / 2 - ch], [ch, lx / 2], [lz - ch, lx / 2], [lz, lx / 2 - ch], [lz, 0]]
        sub = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]
        return contour, sub

    def length_block(self, ext_block=False):
        """
        PM block length 
        :param ext_block: switch for extremity blocks
        :return: pm length
        """
        if ext_block == False:
            l = self.radia_und_param.period / 4 - self.radia_und_param.mag_gap
        else:
            l = (self.radia_und_param.period / 4 - self.radia_und_param.mag_gap) * self.radia_und_param.ext_length_ratio
        return l

    def build_block(self, mag_id=0, period_id=0, ext_block=False):
        """
        Build a pm block
        :param mag_id: identifies the PM block in the period: 0: +H, 1: -V, 2: -H, 3: +V (default: 0)
        :param period_id: identifies the period in the undulator
        :param ext_block: build an extremity block if True (default = False)
        :return: the PM block
        """
        period = self.radia_und_param.period
        # --- Dimensions
        ly = self.length_block(ext_block)
        if ext_block == False:
            pos_y = period_id * period + mag_id * period / 4
        else:
            l = period / 4 - self.radia_und_param.mag_gap
            r = self.radia_und_param.ext_length_ratio
            pos_y = period_id * period + mag_id * period / 4 - l * (1 - r) / 2
        # --- Contour
        contour, sub = self.contour_block()
        # --- Material
        mat_h = rad.MatStd(self.radia_und_param.mat, self.radia_und_param.br_h)
        mat_v = rad.MatStd(self.radia_und_param.mat, self.radia_und_param.br_v)
        # --- Magnetization easy axis
        if mag_id == 0:
            axis = [0, -1, 0]
            mat = mat_h
            color = self.radia_und_param.color_h
        elif mag_id == 1:
            axis = [0, 0, 1]
            mat = mat_v
            color = self.radia_und_param.color_v
        elif mag_id == 2:
            axis = [0, 1, 0]
            mat = mat_h
            color = self.radia_und_param.color_h
        elif mag_id == 3:
            axis = [0, 0, -1]
            mat = mat_v
            color = self.radia_und_param.color_v
        else:
            axis = [0, 0, 0]
            color = [0, 0, 0]
        # --- Build the block (extrusion)
        str_options = 'ki->Numb,TriAngMin->20,TriAreaMax->' + str(self.radia_und_param.area_max)
        pm = rad.ObjMltExtTri(pos_y, ly, contour, sub, 'y', axis, str_options)
        # --- Longitudinal subdivision
        rad.ObjDivMag(pm, [self.radia_und_param.long_sub, 1, 1])
        # --- Set the material
        rad.MatApl(pm, mat)
        # --- Set the color
        rad.ObjDrwAtr(pm, color, self.radia_und_param.thcn)
        # --- Return the block
        return pm

    def build_extremity_hw(self):

        k = int(self.radia_und_param.n_periods / 2)
        # --- build blocks 0 to 3
        pm_0 = self.build_block(0, k)
        pm_1 = self.build_block(1, k)
        pm_2 = self.build_block(2, k)
        pm_3 = self.build_block(3, k, ext_block=True)

        # --- Create a container with all the blocks
        ext = rad.ObjCnt([pm_0, pm_1, pm_2, pm_3])

        # --- Return the extremity
        return ext

    def build_undulator(self, sym):
        """"
        Build a 1/4 of a PPM undulator
        :param sym: apply the symmetries (default = True). Build 1/8 undulator if False.
        :return: the undulator
        """

        # --- Hold the symmetry
        self.sym = sym

        # --- Build an empty container
        und = rad.ObjCnt([])

        # --- Build all the periods
        n_per_half_und = int(self.radia_und_param.n_periods / 2)
        for k in range(n_per_half_und):
            # build blocks 0 to 3
            pm_0 = self.build_block(0, k)
            pm_1 = self.build_block(1, k)
            pm_2 = self.build_block(2, k)
            pm_3 = self.build_block(3, k)
            # Add to the container
            rad.ObjAddToCnt(und, [pm_0, pm_1, pm_2, pm_3])

        # --- Undulator extremity
        ext = self.build_extremity_hw()
        rad.ObjAddToCnt(und, [ext])

        # --- Cut the half of the first magnet
        und = rad.ObjCutMag(und, [0, 0, 0], [0, 1, 0], 'Frame->Lab')[1]

        # --- Move to the specified gap
        tr = rad.TrfTrsl([0, 0, self.radia_und_param.gap / 2])
        rad.TrfOrnt(und, tr)

        # --- Symmetries
        if sym:
            rad.TrfZerPerp(und, [0, 0, 0], [1, 0, 0])
            rad.TrfZerPara(und, [0, 0, 0], [0, 0, 1])
            rad.TrfZerPara(und, [0, 0, 0], [0, 1, 0])

        # --- Return the undulator
        return und

    def print_data(self, short=False):
        """
        Print undulator data
        :param short=False: print a short summary if True
        """
        print('Period: ',self.radia_und_param.period,' mm')
        print('Effective field: ', self.b_eff, ' T')
        print('Peak field: ', self.b_peak, ' T')
        print('Effective K: ', self.k)
        print('Gap: ', self.radia_und_param.gap, ' mm')
        if not short:
            print('Number of periods: ', self.radia_und_param.n_periods)
            print('Material: ',self.radia_und_param.mat)
            print('Br: ', self.radia_und_param.br_h, ' T (H), ', self.radia_und_param.br_v, ' T (V)')
            print('Block height: ', self.radia_und_param.mag_height, 'mm')
            print('Block width: ', self.radia_und_param.mag_width, 'mm')
            print('Block chamfer: ', self.radia_und_param.mag_chamfer, 'mm')

    def build_shim(self, thickness=0.1, width=15, length_to_period=0.5, mag_id=0, period_id=0, x0=0, up=False, 
        sub=[3, 3, 2], color=[0.5,0.5,0.5]):
        """
        Build an iron shim
        :param thickness: shim thickness (mm)
        :param width: shim width (mm)
        :param length_to_period: shim length (longitudinal) to period ratio
        :param mag_id: identifies the PM block in the period: 0: +H, 1: -V, 2: -H, 3: +V 
        :param period_id: identifies the period in the undulator 
        :param x0: transverse position of the centre of the shim
        :param up: select upper (True) or lower (False) magnet assy
        :param sub: subdivision number
        :retun the shim (Radia handle)
        """
        period = self.radia_und_param.period
        gap = self.radia_und_param.gap
        # --- Positions
        y0 = period_id * period + mag_id * period / 4
        z0 = gap / 2 - thickness / 2
        if not up:
            z0 *= -1
        # --- Shim
        shim = rad.ObjRecMag([x0, y0, z0], [width, period * length_to_period, thickness])
        rad.ObjDivMag(shim, sub)
        # --- Material
        mat = rad_mat.set_soft_mat('xc6')
        # --- Set the material
        rad.MatApl(shim, mat)
        # --- Set the color
        rad.ObjDrwAtr(shim, color, self.radia_und_param.thcn)
        # --- Return the block
        return shim

    def add_shim(self, thickness=0.1, width=15, length_to_period=0.5, mag_id=0, period_id=0, x0=0, up=False, 
        sub=[3, 3, 2], color=[0.5,0.5,0.5]):
        """
        Add a single iron shim to the assemnbly
        :param thickness: shim thickness (mm)
        :param width: shim width (mm)
        :param length_to_period: shim length (longitudinal) to period ratio
        :param mag_id: identifies the PM block in the period: 0: +H, 1: -V, 2: -H, 3: +V 
        :param period_id: identifies the period in the undulator 
        :param x0: transverse position of the centre of the shim
        :param up: select upper (True) or lower (False) magnet assy
        :param sub: subdivision number
        """
        shim = self.build_shim(thickness, width, length_to_period, mag_id, period_id, x0, up, sub, color)
        rad.ObjAddToCnt(self.obj, [shim])


# -------------------------------------------
# APPLE2 undulator class
# -------------------------------------------
class APPLE2Undulator(Undulator):
    def __init__(self, radia_und_param, radia_prec_param=None, solve_switch=True, print_switch=True):
        """
        Initialize a Radia undulator
        :param radia_und_param: undulator parameters
        :param radia_prec_param: precision parameters for the Radia solver
        :param solve_switch=True: solve the magnetization problem if True
        :param print_switch=True: quiet mode if False
        """
        # Check the undulator type
        if radia_und_param.und_type != 'apple2':
            print('Error: parameters are not for APPLE2 undulators.')
            return

        # Hold the parameters
        self.radia_und_param = radia_und_param

        # Precision parameters
        if radia_prec_param is None:
            self.radia_prec_param = PrecParams()
        else:
            self.radia_prec_param = radia_prec_param

        # Build
        self.obj = self.build_undulator()

        # Break the symmetry
        self.break_symetry()

        # Apply the phase motion
        tr_pos = rad.TrfTrsl([0, self.radia_und_param.phase / 2, 0])
        tr_neg = rad.TrfTrsl([0, - self.radia_und_param.phase / 2, 0])
        obj_list = rad.ObjCntStuf(self.obj)
        rad.TrfOrnt(obj_list[0], tr_pos)
        rad.TrfOrnt(obj_list[1], tr_pos)
        rad.TrfOrnt(obj_list[2], tr_neg)
        rad.TrfOrnt(obj_list[3], tr_neg)
        rad.TrfOrnt(obj_list[4], tr_neg)
        rad.TrfOrnt(obj_list[5], tr_neg)
        rad.TrfOrnt(obj_list[6], tr_pos)
        rad.TrfOrnt(obj_list[7], tr_pos)

        # Offfset motion to be implemented
        # ...

        # Solve
        if solve_switch:
            self.solve(print_switch=print_switch)

    def contour_block(self):
        """
        Compute magnet contour and associated subdivision
        :return contour, subdivision pairs
        """

        lx = self.radia_und_param.mag_width
        lz = self.radia_und_param.mag_height
        ch = self.radia_und_param.mag_chamfer
        ch0 = self.radia_und_param.mag_chamfer_centre
        s = self.radia_und_param.mag_slot
        dx = self.radia_und_param.transverse_gap / 2

        contour = [[ch0, dx], [0, dx + ch0], [0, dx + lx - ch], [ch, dx + lx], [lz, dx + lx], [lz, dx + s], [lz - s, dx + s], [lz - s, dx]]
        sub = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]
        return contour, sub

    def length_block(self, ext_block=False, ext_block_id=0):
        """
        PM block length 
        :param ext_block: switch for extremity blocks
        :param ext_block_id: index of the extremity block
        :return: pm length
        """
        if ext_block == False:
            l = self.radia_und_param.period / 4 - self.radia_und_param.mag_gap
        else:
            ext = self.radia_und_param.ext
            if 2 * (ext_block_id + 1) > len(ext):
                l = 0
            else:
                l = self.radia_und_param.period * ext[ext_block_id * 2 + 1]
        return l

    def length_gap(self, ext_block_id=0):
        """
        Gap between extremity PM block
        :param ext_block_id: index of the extremity block
        :return: gap length
        """
        ext = self.radia_und_param.ext
        if 2 * ext_block_id >= len(ext):
            l = 0
        else:
            l = self.radia_und_param.period * ext[ext_block_id * 2]
        return l

    def build_block(self, mag_id=0, period_id=0, ext_block=False, ext_block_id=0):
        """
        Build a pm block
        :param mag_id: identifies the PM block in the period: 0: +H, 1: -V, 2: -H, 3: +V
        :param period_id: identifies the period in the undulator
        :param ext_block: build an extremity block if True (default = False)
        :param ext_block_id: extremity block index
        :return: the PM block
        """
        period = self.radia_und_param.period
        # --- Dimensions
        ly = self.length_block(ext_block, ext_block_id)
        if ly <= 0:
            return None

        if ext_block == False:
            pos_y = period_id * period + mag_id * period / 4
        else:
            pos_y = (period_id - 1/4) * period + self.length_block() / 2
            for idx in range(ext_block_id + 1):
                pos_y += self.length_gap(idx)
                pos_y += self.length_block(True, idx)
            pos_y -= ly / 2

        # --- Contour
        contour, sub = self.contour_block()

        # --- Material
        mat_h = rad.MatStd(self.radia_und_param.mat, self.radia_und_param.br_h)
        mat_v = rad.MatStd(self.radia_und_param.mat, self.radia_und_param.br_v)

        # --- Magnetization easy axis
        if mag_id == 0:
            axis = [0, -1, 0]
            mat = mat_h
            color = self.radia_und_param.color_h
        elif mag_id == 1:
            axis = [0, 0, 1]
            mat = mat_v
            color = self.radia_und_param.color_v
        elif mag_id == 2:
            axis = [0, 1, 0]
            mat = mat_h
            color = self.radia_und_param.color_h
        elif mag_id == 3:
            axis = [0, 0, -1]
            mat = mat_v
            color = self.radia_und_param.color_v
        else:
            axis = [0, 0, 0]
            color = [0, 0, 0]

        # --- Build the block (extrusion)
        str_options = 'ki->Numb,TriAngMin->20,TriAreaMax->' + str(self.radia_und_param.area_max)
        pm = rad.ObjMltExtTri(pos_y, ly, contour, sub, 'y', axis, str_options)

        # --- Longitudinal subdivision
        rad.ObjDivMag(pm, [self.radia_und_param.long_sub, 1, 1])

        # --- Set the material
        rad.MatApl(pm, mat)

        # --- Set the color
        rad.ObjDrwAtr(pm, color, self.radia_und_param.thcn)

        # --- Return the block
        return pm

    def build_undulator(self):
        """"
        Build an APPLE2 undulator with zero phase and offset
        :return the undulator
        """

        # --- Build an empty container
        und = rad.ObjCnt([])

        # --- Build all the periods
        n_per_half_und = int(self.radia_und_param.n_periods / 2)
        for k in range(n_per_half_und):
            # build blocks 0 to 3
            pm_0 = self.build_block(0, k)
            pm_1 = self.build_block(1, k)
            pm_2 = self.build_block(2, k)
            pm_3 = self.build_block(3, k)

            # Add to the container
            rad.ObjAddToCnt(und, [pm_0, pm_1, pm_2, pm_3])

        # --- Undulator extremity
        for k in range(len(self.radia_und_param.ext) // 2):
            # Magnet type
            t = k % 4

            # Build the blocks and add to container
            pm = self.build_block(t, n_per_half_und, True, k)

            # Add to the container
            rad.ObjAddToCnt(und, [pm])

        # --- Cut the half of the first magnet
        und = rad.ObjCutMag(und, [0, 0, 0], [0, 1, 0], 'Frame->Lab')[1]

        # --- Move to the specified gap
        tr = rad.TrfTrsl([0, 0, self.radia_und_param.gap / 2])
        rad.TrfOrnt(und, tr)

        # --- Symmetries (broken later)
        rad.TrfZerPara(und, [0, 0, 0], [0, 1, 0])
        rad.TrfZerPerp(und, [0, 0, 0], [1, 0, 0])
        rad.TrfZerPara(und, [0, 0, 0], [0, 0, 1])

        # --- Return the undulator
        return und


# -------------------------------------------
# Planar Hybrid undulator class
# -------------------------------------------
class HybridUndulator(Undulator):
    def __init__(self, radia_und_param, radia_prec_param=None, sym=True, solve_switch=True, print_switch=True):
        """
        Build a hybrid undulator
        :param radia_und_param: undulator parameters
        :param radia_prec_param=None: precision parameters
        :param sym=True: apply the symmetries. Build 1/8 undulator if False.
        :param solve_switch=True: solve the magnetization problem if True
        :param print_switch=True: quiet mode if False
        """
        # Check the undulator type
        if radia_und_param.und_type != 'hybrid':
            print('Error: parameters are not for hybrid undulator.')
            return

        # Hold the parameters
        self.radia_und_param = radia_und_param

        # Precision parameters
        if radia_prec_param is None:
            self.radia_prec_param = PrecParams()
        else:
            self.radia_prec_param = radia_prec_param

        # Build an undulator
        self.obj = self.build_undulator(sym)

        # Solve
        if solve_switch:
            self.solve(print_switch)

    def contour_block(self):
        """
        Compute the block contour and associated subdivisions
        :return: 1/2 block contour, subdivision pairs
        """

        # Block dimensions
        lx_mag = self.radia_und_param.mag_width
        lz_mag = self.radia_und_param.mag_height
        ch_mag = self.radia_und_param.mag_chamfer

        # Contour and subdivisions
        contour = [[0, 0], [0, lx_mag / 2 - ch_mag], [ch_mag, lx_mag / 2], [lz_mag - ch_mag, lx_mag / 2],
                        [lz_mag, lx_mag / 2 - ch_mag], [lz_mag, 0]]

        sub = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]

        return contour, sub
    
    def contour_pole(self):
        """
        Compute the pole contour and associated subdivisions
        :return: 1/2 pole contour, subdivision pairs
        """
        # --- Dimensions
        lx_pole = self.radia_und_param.pole_width
        lz_pole = self.radia_und_param.pole_height
        ch_pole = self.radia_und_param.pole_chamfer
        dy_pole = self.radia_und_param.pole_position
        
        # --- Contour
        contour = [[dy_pole, 0], [dy_pole, lx_pole / 2 - ch_pole], [ch_pole + dy_pole, lx_pole / 2], 
                        [lz_pole + dy_pole, lx_pole / 2], [lz_pole + dy_pole, 0]]
        sub = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]
        
        return contour, sub

    def length_block(self, ext_block=False, mag_id=0):
        """
        PM block length
        :param ext_block: build an extremity block if True 
        :param mag_id: identifies the PM block in the period: 0: +H, 1: -H
        :param period_id: identifies the period in the undulator
        :return: the PM length
        """
        period = self.radia_und_param.period
        m_g = self.radia_und_param.mag_gap
        r = self.radia_und_param.pole_mag_ratio
        if ext_block == False:
            return (period - 4 * m_g) / (2 * (1 + r))
        else:
            return self.length_block_ext(mag_id)

    def length_pole(self, ext_pole=False):
        """
        Pole length
        :param ext_pole: build an extremity block if True 
        :return: the pole length
        """
        if ext_pole == False:
            return self.length_block() * self.radia_und_param.pole_mag_ratio
        else:
            # Unpack the extremity parametres
            w_mag_1, g_1, w_pole, g_2, w_mag_2 = self.radia_und_param.ext
            # Pole thickness
            return w_pole * self.length_pole()

    def length_magnet_ext(self, mag_id):
        """
        Extremity magnet length
        :param mag_id: identifies the PM block in the period: 0: +H, 1: -H
        :return: the pole length
        """
        # Unpack the extremity parametres
        w_mag_0, g_0, w_pole, g_1, w_mag_1 = self.radia_und_param.ext
        # Main magnet thickness
        ly_mag = self.length_block()
        # Main pole thickness
        ly_pole = self.length_pole()
        # First block thickness and position
        ly_mag_0 = w_mag_0 * ly_mag
        # Pole thickness and position
        ly_pole = w_pole * ly_pole
        # Second block thickness and position
        ly_mag_1 = w_mag_1 * ly_mag
        # Thickness and position selector
        if mag_id == 0:
            ly_mag = ly_mag_0
        elif mag_id == 1:
            ly_mag = ly_mag_1
        return ly_mag

    def build_block(self, mag_id=0, period_id=0, ext_block=False):
        """
        Build a pm block for a hybrid undulator
        :param mag_id: identifies the PM block in the period: 0: +H, 1: -H (default: 0)
        :param period_id: identifies the period in the undulator
        :param ext_block: build an extremity block if True (default = False)
        :return: the PM block
        """
        period = self.radia_und_param.period
        # --- Dimensions
        ly_mag = self.length_block()
        # --- Position
        pos_y = mag_id * period / 2 + period_id * period
        # --- Contour
        contour, sub = self.contour_block()
        # --- Material
        mat = rad_mat.set_pm_mat(self.radia_und_param.mag_mat, self.radia_und_param.br)

        # --- Magnetization easy axis
        if mag_id == 0:
            axis = [0, 1, 0]
        elif mag_id == 1:
            axis = [0, -1, 0]
        else:
            return
        # --- Extremity blocks
        if ext_block:
            # Unpack the extremity parametres
            w_mag_0, g_0, w_pole, g_1, w_mag_1 = self.radia_und_param.ext
            # Main pole thickness
            ly_pole = self.length_pole()
            # First block thickness and position
            ly_mag_0 = w_mag_0 * ly_mag
            pos_y_0 = period_id * period - (ly_mag - ly_mag_0) / 2
            # Pole thickness and position
            ly_pole = w_pole * ly_pole
            pos_y_pole = pos_y_0 + ly_mag_0 / 2 + g_0 + ly_pole / 2
            # Second block thickness and position
            ly_mag_1 = w_mag_1 * ly_mag
            pos_y_1 = pos_y_pole +  ly_pole / 2 + g_1 + ly_mag_1 / 2
            # Thickness and position selector
            if mag_id == 0:
                ly_mag = ly_mag_0
                pos_y = pos_y_0
            elif mag_id == 1:
                ly_mag = ly_mag_1
                pos_y = pos_y_1

        # --- Build the block (extrusion)
        str_options = 'ki->Numb,TriAngMin->20,TriAreaMax->' + str(self.radia_und_param.mag_area_max)
        pm = rad.ObjMltExtTri(pos_y, ly_mag, contour, sub, 'y', axis, str_options)
        # --- Longitudinal subdivision
        rad.ObjDivMag(pm, [self.radia_und_param.mag_long_sub, 1, 1])
        # --- Set the material
        rad.MatApl(pm, mat)
        # --- Set the color
        color = self.radia_und_param.mag_color
        rad.ObjDrwAtr(pm, color, self.radia_und_param.thcn)
        # --- Return the block
        return pm

    def build_pole(self, pole_id=0, period_id=0, ext_block=False):
        """
        Build a iron pole for a hybrid undulator
        :param pole_id: identifies the pole in the period: 0: first pole, 1: second pole (default: 0)
        :param period_id: identifies the period in the undulator
        :param ext_block: build an extremity block if True (default = False)
        :return: the PM block
        """

        period = self.radia_und_param.period
        m_g = self.radia_und_param.mag_gap
        r = self.radia_und_param.pole_mag_ratio
        # --- Dimensions
        ly_mag = (period - 4 * m_g) / (2 * (1 + r))
        ly_pole = self.length_pole()
        # --- Longitudinal position
        pos_y = (1 / 2 + pole_id) * period / 2 + period_id * period
        # --- Contour
        contour, sub = self.contour_pole()
        # --- Material
        mat = rad_mat.set_soft_mat(self.radia_und_param.pole_mat)
        # --- Extremity blocks
        if ext_block:
            # Unpack the extremity parametres
            w_mag_1, g_1, w_pole, g_2, w_mag_2 = self.radia_und_param.ext
            # First block thickness
            ly_mag_1 = w_mag_1 * ly_mag
            # Pole thickness
            ly_pole = w_pole * ly_pole
            # position
            pos_y = period_id * period
            pos_y = pos_y - ly_mag /2  + ly_mag_1 + g_1 + ly_pole / 2
        # --- Build the block (extrusion)
        str_options = 'ki->Numb,TriAngMin->20,TriAreaMax->' + str(self.radia_und_param.pole_area_max)
        pole = rad.ObjMltExtTri(pos_y, ly_pole, contour, sub, 'y', str_options)
        # --- Longitudinal subdivision
        rad.ObjDivMag(pole, [self.radia_und_param.pole_long_sub, 1, 1])
        # --- Set the material
        rad.MatApl(pole, mat)
        # --- Set the color
        color = self.radia_und_param.pole_color
        rad.ObjDrwAtr(pole, color, self.radia_und_param.thcn)
        # --- Return the block
        return pole

    def build_extremity(self):
        """
        Build a hybrid undulator extremity
        :return: the extremity
        """

        k = int(self.radia_und_param.n_periods / 2)

        # --- build PM block 0
        pm_0 = self.build_block(0, k, ext_block=True)

        # --- build pole
        pole = self.build_pole(0, k, ext_block=True)

        # --- build PM block 1
        pm_1 = self.build_block(1, k, ext_block=True)

        # --- Create a container with all the blocks
        ext = rad.ObjCnt([pm_0, pole, pm_1])

        # --- Return the extremity
        return ext

    def build_undulator(self, sym=True):
        """
        Build an hybrid undulator
        :param sym: apply the symmetries (default = True). Build 1/8 undulator if False.
        :return: the undulator
        """

        # --- Hold the symmetry
        self.sym = sym

        # --- Build an empty container
        und = rad.ObjCnt([])

        # --- Build all the periods
        n_per_half_und = int(self.radia_und_param.n_periods / 2)
        for k in range(n_per_half_und):
            # build blocks 0 to 3
            mag_0 = self.build_block(0, k)
            mag_1 = self.build_block(1, k)
            pole_0 = self.build_pole(0, k)
            pole_1 = self.build_pole(1, k)
            # Add to the container
            rad.ObjAddToCnt(und, [mag_0, mag_1, pole_0, pole_1])
        # --- Undulator extremity
        mag_ext = self.build_extremity()
        rad.ObjAddToCnt(und, [mag_ext])

        # --- Cut the half of the first magnet
        und = rad.ObjCutMag(und, [0, 0, 0], [0, 1, 0], 'Frame->Lab')[1]

        # --- Move to the specified gap
        tr = rad.TrfTrsl([0, 0, self.radia_und_param.gap / 2])
        rad.TrfOrnt(und, tr)

        # --- Symmetries
        if sym:
            rad.TrfZerPerp(und, [0, 0, 0], [1, 0, 0])
            rad.TrfZerPara(und, [0, 0, 0], [0, 0, 1])
            rad.TrfZerPara(und, [0, 0, 0], [0, 1, 0])

        # --- Return the undulator
        return und

    def print_data(self, short=False):
        """
        Print undulator data
        :param short=False: print a short summary if True
        """
        period = self.radia_und_param.period
        m_g = self.radia_und_param.mag_gap
        r = self.radia_und_param.pole_mag_ratio
        ly_mag = (period - 4 * m_g) / (2 * (1 + r))
        print('Period: ', self.radia_und_param.period, ' mm')
        print('Effective field: ', self.b_eff, ' T')
        print('Peak field: ', self.b_peak, ' T')
        print('Effective K: ', self.k)
        print('Gap: ', self.radia_und_param.gap, ' mm')
        if not short:
            print('Number of periods: ', self.radia_und_param.n_periods)
            print('PM Material: ', self.radia_und_param.mag_mat)
            print('Br: ', self.radia_und_param.br, ' T')
            print('PM block length: ', ly_mag , ' mm')
            print('PM block height: ', self.radia_und_param.mag_height, 'mm')
            print('PM block width: ', self.radia_und_param.mag_width, 'mm')
            print('PM block chamfer: ', self.radia_und_param.mag_chamfer, 'mm')
            print('Pole material: ', self.radia_und_param.pole_mat)
            print('Pole-to-mag length ratio: ', r )
            print('Pole length: ', ly_mag * r, ' mm')
            print('Pole height: ', self.radia_und_param.pole_height, 'mm')
            print('Pole width: ', self.radia_und_param.pole_width, 'mm')
            print('Pole chamfer: ',self.radia_und_param.pole_chamfer, ' mm')


# -------------------------------------------
# Planar Hybrid wiggler class
# -------------------------------------------
class HybridWiggler(Undulator):
    def __init__(self, radia_und_param, radia_prec_param=None, sym=True, solve_switch=True, print_switch=True):
        # Hold parameters
        self.radia_und_param = radia_und_param

        # Precision parameters
        if radia_prec_param is None:
            self.radia_prec_param = PrecParams()
        else:
            self.radia_prec_param = radia_prec_param

        # Build an undulator
        self.obj = self.build_undulator(sym)

        # Solve
        if solve_switch:
            self.solve(print_switch)

    def contour_block_main(self):
        """
        Compute PM block contour
        :return: contour, subdivision
        """
        lx_mag = self.radia_und_param.mag_width
        lz_mag = self.radia_und_param.mag_height[0]
        ch_mag = self.radia_und_param.mag_chamfer
        contour = [[0, 0], [0, lx_mag / 2 - ch_mag], [ch_mag, lx_mag / 2], [lz_mag - ch_mag, lx_mag / 2],
                        [lz_mag, lx_mag / 2 - ch_mag], [lz_mag, 0]]
        sub = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]
        return contour, sub

    def contour_block_side(self):
        """
        Compute side PM block contour
        :return contour, subdivision
        """
        lx_mag = self.radia_und_param.mag_width
        lz_mag = self.radia_und_param.mag_height[1]
        ch_mag = self.radia_und_param.mag_chamfer
        p_width = self.radia_und_param.pole_width
        contour = [[0, p_width / 2], [0, lx_mag / 2 - ch_mag], [ch_mag, lx_mag / 2], [lz_mag - ch_mag, lx_mag / 2],
                        [lz_mag, lx_mag / 2 - ch_mag], [lz_mag, p_width / 2]]
        sub = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]
        return contour, sub
        
    def contour_pole(self):
        """
        Compute pole contour
        :return contour, subdivision
        """
        # --- Dimensions
        lx_pole = self.radia_und_param.pole_width
        ly_pole = self.radia_und_param.pole_length
        lz_pole = self.radia_und_param.pole_height
        # --- Contour
        contour = [[0, 0], [0, lx_pole / 2], [lz_pole, lx_pole / 2], [lz_pole, 0]]
        sub = [[1, 1], [1, 1], [1, 1],[1, 1]]

        return contour, sub
    
    def contour_pole_side_ext(self):
        # --- Dimensions
        p_width = self.radia_und_param.pole_width
        lx_mag = self.radia_und_param.mag_width
        lz_mag = self.radia_und_param.mag_height[1]
        ch_mag = self.radia_und_param.mag_chamfer
        # --- Contour
        contour = [[0, p_width / 2], [0, lx_mag / 2 - ch_mag], [ch_mag, lx_mag / 2], [lz_mag - ch_mag, lx_mag / 2],
                        [lz_mag, lx_mag / 2 - ch_mag], [lz_mag, p_width / 2]]
        sub = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]

        return contour, sub

    def contour_block_ext(self):
        """
        Compute the contour of an external block
        :return contour, subdivision
        """
        # --- Dimensions
        lx_mag = self.radia_und_param.mag_width
        lz_mag = self.radia_und_param.mag_height[0]
        ch_mag = self.radia_und_param.mag_chamfer
        # --- Contour
        contour = [[0, 0], [0, lx_mag / 2 - ch_mag], [ch_mag, lx_mag / 2], [lz_mag - ch_mag, lx_mag / 2],
                        [lz_mag, lx_mag / 2 - ch_mag], [lz_mag, 0]]
        sub = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]
        
        return contour, sub

    def length_block(self, ext_block=False):
        """
        Block length
        :param ext_block: extremity block if True
        :return: block length
        """
        if ext_block == False:
            return self.length_block_main()
        else:
            return self.length_block_ext()

    def length_block_main(self):
        """
        Length of the main block
        """
        period = self.radia_und_param.period
        p_len = self.radia_und_param.pole_length
        return (period - 2 * p_len) / 4

    def length_block_ext(self):
        """
        Extremity magnet length
        :return: magnet length
        """
        return self.radia_und_param.ext_mag[0]

    def length_pole(self):
        """
        Pole length
        :return: the pole length
        """
        return self.radia_und_param.pole_length

    def length_pole_ext(self):
        """
        Extremity pole length
        :return: pole length
        """
        return self.radia_und_param.ext_pole[0]

    def build_block_main(self, mag_id=0, period_id=0):
        """
        Build a pm block for a hybrid wiggler
        :param mag_id: identifies the PM block in the period: 0: +H, 1: -H, 2: -H, 3: +H (default: 0)
        :param period_id: identifies the period in the undulator
        :return: the PM block
        """

        period = self.radia_und_param.period
        p_len = self.radia_und_param.pole_length
        # --- Dimensions
        lx_mag = self.radia_und_param.mag_width
        ly_mag = self.length_block_main()
        ch_mag = self.radia_und_param.mag_chamfer
        # --- Position
        if mag_id == 0:
            pos_y = ly_mag / 2 + period_id * period
        elif mag_id == 1:
            pos_y = 1.5 * ly_mag + p_len + period_id * period
        elif mag_id == 2:
            pos_y = 2.5 * ly_mag + p_len + period_id * period
        elif mag_id == 3:
            pos_y = 3.5 * ly_mag + 2 * p_len + period_id * period
        else:
            print('Warning: wrong magnet id')
            pos_y = 0
        # --- Contour
        contour, sub = self.contour_block_main()
        # --- Material
        mat = rad_mat.set_pm_mat(self.radia_und_param.mag_mat, self.radia_und_param.br)
        # --- Magnetization easy axis
        if mag_id == 0 or mag_id == 3:
            axis = [0, 1, 0]
        elif mag_id == 1 or mag_id == 2:
            axis = [0, -1, 0]
        else:
            axis = [0, 1, 0]
        # --- Build the block (extrusion)
        str_options = 'ki->Numb,TriAngMin->20,TriAreaMax->' + str(self.radia_und_param.mag_area_max)
        pm = rad.ObjMltExtTri(pos_y, ly_mag, contour, sub, 'y', axis, str_options)
        # --- Longitudinal subdivision
        rad.ObjDivMag(pm, [self.radia_und_param.mag_long_sub, 1, 1])
        # --- Chamfers
        if mag_id == 0:
            pos_ch = [lx_mag / 2 - ch_mag, period_id * period, 0]
            n_ch = [1, -1, 0]
        elif mag_id == 1:
            pos_ch = [lx_mag / 2 - ch_mag, period_id * period + p_len + 2 * ly_mag, 0]
            n_ch = [1, 1, 0]
        elif mag_id == 2:
            pos_ch = [lx_mag / 2 - ch_mag, period_id * period + p_len + 2 * ly_mag, 0]
            n_ch = [1, -1, 0]
        elif mag_id == 3:
            pos_ch = [lx_mag / 2 - ch_mag, (period_id + 1) * period, 0]
            n_ch = [1, 1, 0]
        else:
            pos_ch = [lx_mag / 2 - ch_mag, period_id * period, 0]
            n_ch = [-1, 1, 0]
        pm = rad.ObjCutMag(pm, pos_ch, n_ch, 'Frame->Lab')[0]
        # --- Set the material
        rad.MatApl(pm, mat)
        # --- Set the color
        color = self.radia_und_param.mag_color[0]
        rad.ObjDrwAtr(pm, color)
        # --- Return the block
        return pm

    def build_block_side(self, mag_id=0, period_id=0):
        """
        Build a side PM block for hybrid wiggler
        :param mag_id: block type : 0 (-X) or 1 (+X)
        :param period_id: period number
        :return: pm block
        """
        period = self.radia_und_param.period
        p_len = self.radia_und_param.pole_length
        # --- Dimensions
        ly_mag = self.length_block_main()
        # --- Position and axis
        if mag_id == 0:
            pos_y = ly_mag + p_len / 2 + period_id * period
            axis = [-1, 0, 0]
        elif mag_id == 1:
            pos_y = 3 * ly_mag + 1.5 * p_len  + period_id * period
            axis = [1, 0, 0]
        else:
            print('Warning: wrong pole id')
            pos_y = 0
        # --- Contour
        contour, sub = self.contour_block_side()
        # --- Material
        mat = rad_mat.set_pm_mat(self.radia_und_param.mag_mat, self.radia_und_param.br)
        # --- Build the block (extrusion)
        str_options = 'ki->Numb,TriAngMin->20,TriAreaMax->' + str(self.radia_und_param.mag_area_max)
        pm = rad.ObjMltExtTri(pos_y, p_len, contour, sub, 'y', axis, str_options)
        # --- Longitudinal subdivision
        rad.ObjDivMag(pm, [self.radia_und_param.pole_long_sub, 1, 1])
        # --- Set the material
        rad.MatApl(pm, mat)
        # --- Set the color
        color = self.radia_und_param.mag_color[1]
        rad.ObjDrwAtr(pm, color)
        # --- Return the block
        return pm

    def build_pole(self, pole_id=0, period_id=0):
        """
        Build a iron pole for a hybrid undulator
        :param pole_id: identifies the pole in the period: 0: first pole, 1: second pole (default: 0)
        :param period_id: identifies the period in the undulator
        :return: the pole
        """
        period = self.radia_und_param.period
        # --- Dimensions
        ly_pole = self.length_pole()
        # --- Longitudinal position
        pos_y = (1 / 2 + pole_id) * period / 2 + period_id * period
        # --- Contour
        contour, sub = self.contour_pole()
        # --- Material
        mat = rad_mat.set_soft_mat(self.radia_und_param.pole_mat)
        # --- Build the block (extrusion)
        str_options = 'ki->Numb,TriAngMin->20,TriAreaMax->' + str(self.radia_und_param.pole_area_max)
        pole = rad.ObjMltExtTri(pos_y, ly_pole, contour, sub, 'y', str_options)
        # --- Longitudinal subdivision
        rad.ObjDivMag(pole, [self.radia_und_param.pole_long_sub, 1, 1])
        # --- Set the material
        rad.MatApl(pole, mat)
        # --- Set the color
        color = self.radia_und_param.pole_color
        rad.ObjDrwAtr(pole, color)
        # --- Return the block
        return pole

    def build_block_side_ext(self):
        """
        Build a extremity side PM block for hybrid wiggler
        :return: pm block
        """

        period = self.radia_und_param.period
        p_len = self.radia_und_param.pole_length
        period_id = int(self.radia_und_param.n_periods / 2)
        pole_ext_length = self.length_pole_ext()
        d = self.radia_und_param.ext_pole[1]
        # --- Position and axis
        ly_mag = (period - 2 * p_len) / 4
        pos_y = ly_mag + pole_ext_length / 2 + period_id * period + d
        axis = [-1, 0, 0]
        # --- Contour
        contour, sub = self.contour_pole_side_ext()
        # --- Material
        mat = rad_mat.set_pm_mat(self.radia_und_param.mag_mat, self.radia_und_param.br)
        # --- Build the block (extrusion)
        str_options = 'ki->Numb,TriAngMin->20,TriAreaMax->' + str(self.radia_und_param.mag_area_max)
        pm = rad.ObjMltExtTri(pos_y, pole_ext_length, contour, sub, 'y', axis, str_options)
        # --- Longitudinal subdivision
        rad.ObjDivMag(pm, [self.radia_und_param.pole_long_sub, 1, 1])
        # --- Set the material
        rad.MatApl(pm, mat)
        # --- Set the color
        color = self.radia_und_param.mag_color[1]
        rad.ObjDrwAtr(pm, color)
        # --- Return the block
        return pm

    def build_pole_ext(self):
        """
        Build an extremity iron pole for a hybrid undulator
        :return: the pole
        """

        period = self.radia_und_param.period
        pole_ext_length = self.length_pole_ext()
        d = self.radia_und_param.ext_pole[1]
        period_id = int(self.radia_und_param.n_periods / 2)
        # --- Dimensions
        ly_pole = self.radia_und_param.pole_length
        # --- Longitudinal position
        ly_mag = (period - 2 * ly_pole) / 4
        pos_y = ly_mag + pole_ext_length / 2 + period_id * period + d
        # --- Contour
        contour, sub = self.build_pole()
        # --- Material
        mat = rad_mat.set_soft_mat(self.radia_und_param.pole_mat)
        # --- Build the block (extrusion)
        str_options = 'ki->Numb,TriAngMin->20,TriAreaMax->' + str(self.radia_und_param.pole_area_max)
        pole = rad.ObjMltExtTri(pos_y, pole_ext_length, contour, sub, 'y', str_options)
        # --- Longitudinal subdivision
        rad.ObjDivMag(pole, [self.radia_und_param.pole_long_sub, 1, 1])
        # --- Set the material
        rad.MatApl(pole, mat)
        # --- Set the color
        color = self.radia_und_param.pole_color
        rad.ObjDrwAtr(pole, color)
        # --- Return the block
        return pole

    def build_block_ext(self):
        """
        Build a extremity PM block for hybrid wiggler
        :return: pm block
        """
        period = self.radia_und_param.period
        p_len = self.radia_und_param.pole_length
        period_id = int(self.radia_und_param.n_periods / 2)
        pole_ext_length = self.length_pole_ext()
        mag_ext_length = self.length_block_ext()
        d_pole = self.radia_und_param.ext_pole[1]
        d_mag = self.radia_und_param.ext_mag[1]
        # --- Dimensions
        lx_mag = self.radia_und_param.mag_width
        ch_ext = self.radia_und_param.ext_mag[2]
        # --- Position and axis
        ly_mag = (period - 2 * p_len) / 4
        pos_y = ly_mag + pole_ext_length  + period_id * period + d_pole + d_mag + mag_ext_length / 2
        axis = [0, -1, 0]
        # --- Contour
        contour, sub = self.contour_block_ext()
        # --- Material
        mat = rad_mat.set_pm_mat(self.radia_und_param.mag_mat, self.radia_und_param.br)
        # --- Build the block (extrusion)
        str_options = 'ki->Numb,TriAngMin->20,TriAreaMax->' + str(self.radia_und_param.mag_area_max)
        pm = rad.ObjMltExtTri(pos_y, mag_ext_length, contour, sub, 'y', axis, str_options)
        # --- Chamfers
        pos_ch = [lx_mag / 2, pos_y + mag_ext_length / 2 - ch_ext, 0]
        n_ch = [1, 1, 0]
        pm = rad.ObjCutMag(pm, pos_ch, n_ch, 'Frame->Lab')[0]

        # --- Longitudinal subdivision
        rad.ObjDivMag(pm, [self.radia_und_param.pole_long_sub, 1, 1])
        # --- Set the material
        rad.MatApl(pm, mat)
        # --- Set the color
        color = self.radia_und_param.mag_color[0]
        rad.ObjDrwAtr(pm, color)
        # --- Return the block
        return pm

    def build_undulator(self, sym=True):
        """
        Build an hybrid undulator
        :param sym: apply the symmetries (default = True). Build 1/8 undulator if False.
        :return: the undulator
        """

        # --- Hold the symmetry
        self.sym = sym

        # --- Build an empty container
        und = rad.ObjCnt([])

        # --- Build all the periods
        n_per_half_und = int(self.radia_und_param.n_periods / 2)
        for k in range(n_per_half_und):
            # build blocks 0 to 3
            mag_0 = self.build_block_main(0, k)
            mag_1 = self.build_block_main(1, k)
            mag_2 = self.build_block_main(2, k)
            mag_3 = self.build_block_main(3, k)
            mag_side_0 = self.build_block_side(0, k)
            mag_side_1 = self.build_block_side(1, k)
            pole_0 = self.build_pole(0, k)
            pole_1 = self.build_pole(1, k)

            # Add to the container
            rad.ObjAddToCnt(und, [mag_0, mag_1, mag_2, mag_3, mag_side_0, mag_side_1, pole_0, pole_1])

        # --- Undulator extremity
        mag_main_ext = self.build_block_main(0, n_per_half_und) # Last standard magnet
        mag_side_ext = self.build_block_side_ext() # Extremity side magnet block
        pole_ext = self.build_pole_ext() # Extremity pole
        mag_ext = self.build_block_ext()
        rad.ObjAddToCnt(und, [mag_main_ext, mag_side_ext, pole_ext, mag_ext])

        # --- Cut the half of the first magnet
        # und = rad.ObjCutMag(und, [0, 0, 0], [0, 1, 0], 'Frame->Lab')[1]

        # --- Move to the specified gap
        tr = rad.TrfTrsl([0, 0, self.radia_und_param.gap / 2])
        rad.TrfOrnt(und, tr)

        # --- Symmetries
        if sym:
            rad.TrfZerPerp(und, [0, 0, 0], [1, 0, 0])
            rad.TrfZerPara(und, [0, 0, 0], [0, 0, 1])
            rad.TrfZerPara(und, [0, 0, 0], [0, 1, 0])

        # --- Return the undulator
        return und


# -------------------------------------------
# Staggered undulator
# -------------------------------------------
class StaggeredUndulator(Undulator):
    def __init__(self, radia_und_param, radia_prec_param=None, solenoid=None, solve_switch=True,
                 print_switch=True):
        """
        Initialize a staggered undulator
        :param radia_und_param: staggered undulator parameters
        :param radia_prec_param: precision parameters for the radia solver
        :param solenoid=None: solenoid (use background field if None)
        :param solve_switch: solve the magnetization problem if True
        :param print_switch: quiet mode if False
        """
        # Check the undulator type
        if radia_und_param.und_type != 'staggered':
            print('Error: parameters are not for staggered undulator.')
            return

        # Hold the parameters
        self.radia_und_param = radia_und_param

        # Precision parameters
        if radia_prec_param is None:
            self.radia_prec_param = PrecParams()
        else:
            self.radia_prec_param = radia_prec_param

        # Solenoid
        if solenoid is None:
            self.solenoid = rad.ObjBckg([0, self.radia_und_param.sol_field, 0])
        else:
            self.solenoid = solenoid

        # Build an undulator
        self.obj = self.build_undulator()

        # Solve
        if solve_switch:
            self.solve(print_switch=print_switch)

    def length_block(self):
        """
        PM block length
        """
        return None
    
    def length_pole(self):
        """
        Pole length
        :return: pole length
        """
        return self.radia_und_param.period * self.radia_und_param.pole_period_ratio
       
    def build_pole(self, pole_id=0, period_id=0, ext_pole=False):
        """
        Build a iron pole for a hybrid undulator
        :param pole_id=0: identifies the pole in the period: 0: upper pole, 1: lower pole
        :param period_id=0: identifies the period in the undulator
        :param ext_block=False: build an extremity block if True
        :return: the pole
        """

        period = self.radia_und_param.period
        g = self.radia_und_param.gap
        # --- Dimensions
        lx_pole = self.radia_und_param.pole_width
        ly_pole = self.length_pole()
        lz_pole = self.radia_und_param.pole_height
        # --- Longitudinal position
        pos_y = period_id * period + pole_id * period / 2
        # --- Contour
        if pole_id == 0:
            s = 1
        else:
            s = -1
        contour = [[s * g / 2, 0], [s * g / 2, lx_pole / 2], [s * (lz_pole + g / 2), lx_pole / 2],
                        [s * (lz_pole + g / 2), 0]]
        sub = [[self.radia_und_param.pole_sub, 1], [1, 1], [1, 1], [1, 1]]
        # --- Material
        mat = rad_mat.set_soft_mat(self.radia_und_param.pole_mat)
        # --- Extremity blocks
        if ext_pole:
            # Unpack the extremity parametres
            g_ext, l_ext = self.radia_und_param.ext
            # Position
            pos_y += g_ext
            # Pole thickness
            ly_pole *= l_ext
        # --- Build the block (extrusion)
        str_options = 'ki->Numb,TriAngMin->20,TriAreaMax->' + str(self.radia_und_param.pole_area_max)
        pole = rad.ObjMltExtTri(pos_y, ly_pole, contour, sub, 'y', str_options)
        # --- Longitudinal subdivision
        rad.ObjDivMag(pole, [self.radia_und_param.pole_long_sub, 1, 1])
        # --- Set the material
        rad.MatApl(pole, mat)
        # --- Set the color
        color = self.radia_und_param.pole_color
        rad.ObjDrwAtr(pole, color, self.radia_und_param.thcn)
        # --- Return the block
        return pole

    def build_undulator(self):
        """
        Build a staggered undulator
        :return: the undulator
        """

        # --- Build an empty container
        poles = rad.ObjCnt([])

        # --- Build all the periods
        n_per_half_und = int(self.radia_und_param.n_periods / 2)
        for k in range(n_per_half_und):
            # Upper and lower poles
            pole_up = self.build_pole(0, k)
            pole_down = self.build_pole(1, k)
            # Add to the container
            rad.ObjAddToCnt(poles, [pole_up, pole_down])
        # --- Undulator extremity
        pole_ext = self.build_pole(0, n_per_half_und, ext_pole=True)
        rad.ObjAddToCnt(poles, [pole_ext])

        # --- Cut the half of the first pole
        poles = rad.ObjCutMag(poles, [0, 0, 0], [0, 1, 0], 'Frame->Lab')[1]

        # --- Symmetries
        rad.TrfZerPerp(poles, [0, 0, 0], [1, 0, 0])
        rad.TrfZerPara(poles, [0, 0, 0], [0, 1, 0])

        # --- Add the solenoid field
        und = rad.ObjCnt([self.solenoid])
        rad.ObjAddToCnt(und, [poles])

        # --- Return the undulator
        return und

    def field_sol(self, xyz_end, xyz_start=[0, 0, 0], n=100, b='bz'):
        # --- Sampling
        x0, y0, z0 = xyz_start
        x1, y1, z1 = xyz_end
        # Steps
        dx = (x1 - x0) / (n - 1)
        dy = (y1 - y0) / (n - 1)
        dz = (z1 - z0) / (n - 1)
        # Positions
        x = [x0 + k * dx for k in range(n)]
        y = [y0 + k * dy for k in range(n)]
        z = [z0 + k * dz for k in range(n)]
        xyz = [[x[k], y[k], z[k]] for k in range(n)]
        # Distance to initial point
        d = [((x[k] - x[k - 1]) ** 2 + (y[k] - y[k - 1]) ** 2 + (z[k] - z[k - 1]) ** 2) ** 0.5 for k in range(1, n)]
        d = list(accumulate(d))
        d.insert(0, 0)

        # --- Field computation
        bz = rad.Fld(self.solenoid, b, xyz)

        # --- Return
        return x, y, z, d, bz

    def plot_field_sol(self, xyz_end, xyz_start=[0, 0, 0], n=100, b='bz', x_axis='d', plot_show=True, plot_title=''):
        """"
        Compute and plot the field along a straight line
        :param xyz_end: end point [x, y, z]
        :param xyz_start: starting point [x, y, z] (default = [0, 0, 0])
        :param n: number of points (default = 100)
        :param b: field component (default = 'bz')
        :param xaxis: defines the x axis of the plot, x_axis = 'x', 'y', 'z' or 'd' (default = 'd', i.e. distance)
        :param show: show the plot if True
        :param plot_title: plot title
        :return: a matplotlib figure
        """

        # --- Compute the field
        x, y, z, d, bz = self.field_sol(xyz_end=xyz_end, xyz_start=xyz_start, n=n, b=b)
        # --- Plot
        fig = figure()
        if x_axis == 'x':
            l = x
        elif x_axis == 'y':
            l = y
        elif x_axis == 'z':
            l = z
        else:
            l = d
        plot(l, bz)
        xlabel('Distance (mm)')
        ylabel('Field (T)')
        title(plot_title)
        if plot_show:
            show()
        return fig

    def print_data(self, short=False):
        """
        Print undulator data
        :param short=False: print a short summary if True
        """
        print('Period: ', self.radia_und_param.period, ' mm')
        print('Effective field: ', self.b_eff, ' T')
        print('Peak field: ', self.b_peak, ' T')
        print('Effective K: ', self.k)
        print('Gap: ', self.radia_und_param.gap, ' mm')
        print('Solenoid field: ', self.radia_und_param.sol_field, ' T')
        if not short:
            print('Number of periods: ', self.radia_und_param.n_periods)
            print('Material: ', self.radia_und_param.pole_mat)
            print('Pole height: ', self.radia_und_param.pole_height, 'mm')
            print('Pole width: ', self.radia_und_param.pole_width, 'mm')


# -------------------------------------------
# PPM Phase shifter
# -------------------------------------------
class PPMPhaseShifter(Undulator):
    def __init__(self, radia_ps_param, radia_prec_param=None, sym=True, solve_switch=True, print_switch=True):
        """
        Initialize a PPM phase shifter
        :param radia_ps_param: phase shifter parameters
        :param radia_prec_param: precision parameters for the Radia solver
        :param sym: apply the symmetries (default = True). Build 1/8 undulator if False.
        :param solve_switch=True: solve the magnetization problem if True
        :param print_switch=True: quiet mode if False
        """
        # Check the undulator type
        if radia_ps_param.und_type != 'ppm_ps':
            print('Error: parameters are not for PPM phase shifter.')
            return

        # Hold the parameters
        self.radia_und_param = radia_ps_param

        # Precision parameters
        if radia_prec_param is None:
            self.radia_prec_param = PrecParams()
        else:
            self.radia_prec_param = radia_prec_param

        # Build an undulator
        self.obj = self.build_undulator(sym)

        # Solve
        if solve_switch:
            self.solve(print_switch=print_switch)

    def contour_block(self):
        """
        Compute the contour of PM blocks
        :return contour, subdivisions
        """
         # --- Dimensions
        lx = self.radia_und_param.mag_width
        lz = self.radia_und_param.mag_height
        ch = self.radia_und_param.mag_chamfer
        # --- Contour
        contour = [[0, 0], [0, lx / 2 - ch], [ch, lx / 2], [lz - ch, lx / 2], [lz, lx / 2 - ch], [lz, 0]]
        sub = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]

        return contour, sub

    def length_block(self):
        """
        Block length
        :return: PM length
        """
        return self.radia_und_param.period / 4 - self.radia_und_param.mag_gap

    def build_block(self, mag_id=0):
        """
        Build a pm block
        :param mag_id=0: identifies the PM block in the period: 0: -V, 1: -H, 2: -H(double_h=True) or +V, 3: +V
        :return: the PM block
        """

        period = self.radia_und_param.period
        # --- Dimensions
        ly = self.length_block()
        pos_y = period / 8 + mag_id * period / 4 + self.radia_und_param.dist[0] / 2
        # --- Contour
        contour, sub = self.contour_block()
        # --- Material
        mat_h = rad.MatStd(self.radia_und_param.mat, self.radia_und_param.br_h)
        mat_v = rad.MatStd(self.radia_und_param.mat, self.radia_und_param.br_v)
        # --- Magnetization easy axis
        if mag_id == 0:
            axis = [0, 0, -1]
            mat = mat_v
            color = self.radia_und_param.color_v
        elif mag_id == 1:
            axis = [0, -1, 0]
            mat = mat_h
            color = self.radia_und_param.color_h
        elif mag_id == 2:
            if self.radia_und_param.double_h:
                axis = [0, -1, 0]
                mat = mat_h
                color = self.radia_und_param.color_h
            else:
                axis = [0, 0, 1]
                mat = mat_v
                color = self.radia_und_param.color_v
                pos_y += self.radia_und_param.dist[1]
        elif mag_id == 3:
            axis = [0, 0, 1]
            mat = mat_v
            color = self.radia_und_param.color_v
            pos_y += self.radia_und_param.dist[1]
        else:
            axis = [0, 0, 0]
            color = [0, 0, 0]
        # --- Build the block (extrusion)
        str_options = 'ki->Numb,TriAngMin->20,TriAreaMax->' + str(self.radia_und_param.area_max)
        pm = rad.ObjMltExtTri(pos_y, ly, contour, sub, 'y', axis, str_options)
        # --- Longitudinal subdivision
        rad.ObjDivMag(pm, [self.radia_und_param.long_sub, 1, 1])
        # --- Set the material
        rad.MatApl(pm, mat)
        # --- Set the color
        rad.ObjDrwAtr(pm, color, self.radia_und_param.thcn)
        # --- Return the block
        return pm

    def build_undulator(self, sym):
        """"
        Build a 1/4 of a PPM undulator
        :param sym: apply the symmetries (default = True). Build 1/8 undulator if False.
        :return: the undulator
        """

        # --- Hold the symmetry
        self.sym = sym

        # --- Build an empty container
        und = rad.ObjCnt([])

        # --- Build the blocks
        # Number of blocks
        if self.radia_und_param.double_h:
            n_blocks = 4
        else:
            n_blocks = 3
        # Build the blocks
        for k in range(n_blocks):
            pm = self.build_block(k)
            # Add to the container
            rad.ObjAddToCnt(und, [pm])

        # --- Move to the specified gap
        tr = rad.TrfTrsl([0, 0, self.radia_und_param.gap / 2])
        rad.TrfOrnt(und, tr)

        # --- Symmetries
        if sym:
            rad.TrfZerPerp(und, [0, 0, 0], [1, 0, 0])
            rad.TrfZerPara(und, [0, 0, 0], [0, 0, 1])
            rad.TrfZerPerp(und, [0, 0, 0], [0, 1, 0])

        # --- Return the phase shifter
        return und
