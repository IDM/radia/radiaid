# ----------------------------------------------------------
# radia_und_test.py
# Tests and exemples for the PyRadiaUndulators library
#
# Gael Le Bec, ESRF, 2019
# ----------------------------------------------------------

# --- Imports
from radia_id import *

# ------------------
# Tests
# ------------------
# --- Switches
sim_ppm = True # Run the PPM undulator model
sim_hyb = False # Run the hybrid undulator model
sym_wig = False # Run a hybrid wiggler model
sym_stag = False # Run a staggered undulator model
plot_model3d = False # 3D Plot of the Radia model
plot_field_profiles = False # Plot the field profiles
plot_trajectory = False # Plot the trajectory of a particle
und_save = False # Save the model
und_load = False # Load the model

# ------------------------------------------
# Simulate a PPM undulator
if sim_ppm:
    # --- Precision parameters
    prec_param = PrecParams(0.0001, 1000)
    # --- Undulator parameters
    ppm_param = PPMUndParam(26, 4, 11)

    # --- Build
    und = PPMUndulator(ppm_param, prec_param)
    # --- Print the main undulator data
    und.print_data()
    # --- Print the radiation wavelength
    und.print_wavelength(6)

    # --- Plot the geometry
    if plot_model3d:
        und.plot_geo()

    # --- Field profiles
    if plot_field_profiles:
        # Longitudinal field profile
        pos_end = [0, ppm_param.period * (ppm_param.n_periods /2 + 1), 0]
        und.plot_field(pos_end, [0,-100, 0],plot_show=False, x_axis='y',plot_title='Longitudinal profile')
        # Transverse field profile
        fig_x = und.plot_field([50, ppm_param.period / 4, 0], [-50, ppm_param.period /4 , 0], plot_show=False,
                               x_axis='x', plot_title='Transverse profile')
        # Vertical profile
        fig_z = und.plot_field([0, ppm_param.period / 4, 5], [0, ppm_param.period / 4, -5], x_axis='z',
                               plot_title='Vertical profile')

    # --- Trajectory
    if plot_trajectory:
        e_beam = 6 # Energy of the electron beam
        und.plot_traj(e_beam, plot_title='Trajectory')

    # # --- Change the material
    # ppm_param_2 = PPMUndParam(35, 4, 11, mat='sm2co17', br_v=1.1, br_h=1.1)
    # und_2 = PPMUndulator(ppm_param_2, prec_param)
    # und_2.print_data()
    #
    # # --- Simple plot
    # if plot_field_profiles:
    #     und_2.plot_field([0, 100, 0])
    #
    # # --- Change the gap
    # und_2.set_gap(20)
    # und_2.print_data()

    # --- Wait
    # if plot_field_profiles == False:
    #    input()

# ------------------------------------------
# Simulate an bybrid undulator
if sim_hyb:
    # --- Precision parameters
    prec_param = PrecParams(0.0001, 1000)
    # --- Undulator parameters
    hyb_param = HybridUndParam(18, 4, 6, br=1.6, pole_mat='fecov')

    # --- Build
    und = HybridUndulator(hyb_param, prec_param)

    # --- Print the main undulator data
    und.print_data()

    # --- Plot the geometry
    und.plot_geo()

    # --- Plot the field
    und.plot_field([0, 100, 0])

    # --- Save
    if und_save:
        und.save('test')

# -----------------------------------------
# Simulate an hybrid wiggler
if sym_wig:
    param = HybridWigParam(period=150,n_periods=3,gap=20)
    wig = HybridWiggler(param, solve_switch=True)
    wig.plot_geo()
    wig.plot_field([0, 300, 0], [0, -300, 0])

# -----------------------------------------
# Simulate an staggered undulator
if sym_stag:
    param = StaggeredUndParam(period=8, n_periods=10, gap=4, sol_field=1, pole_period_ratio=0.57)
    stag = StaggeredUndulator(param, solve_switch=True)
    stag.print_data()
    stag.plot_geo()
    stag.plot_field([0, 50, 0], n=200, b='by')


# ------------------------------------------
# Load an undulator model
if und_load:

    # --- Load
    und = Undulator()
    und.load('test')

    # --- Plot the field
    und.plot_field([0, 100, 0])


