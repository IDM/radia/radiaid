# PyRadiaUndulators

Python library for building undulator RADIA models. It can be used for building various undulator in Python. The undulators come as Python objects with longitudinal field plots, 3D geometry plots and other methods. 


Gael Le Bec, ESRF, 2019 - 2020

## Dependencies

[Radia] Python version of the Radia magnetostatic simulation code, https://github.com/ochubar/Radia

[matplotlib] Plot figures with Python

[RadiaUtils] Python auxiliary functions for radia (magnetic materials, save and load, etc.)

## Installation

No setup file is provided from now, but the installation is rather simple:

- Clone the repository where you want
- Navigate to your python *Lib/site-packages* folder
- Create a *radia_id.pth* file containing the path to the repository (note that the path to the *radia_utils* can be included in the same file)

## 3D objects vizualization available with Windows only

The present version of Radia do not allow to display 3D objects with Linux and MacOSX. It will be integrated in the *.plot_geo()* method when available. A VTK visualization tool was developed by RadiaSoft: https://github.com/radiasoft/Radia-Examples. 

## Exemples

Please see the script *und_test.py* for exemples.

Basic Jupyter notebooks are available: 
- Undulators_PPM.ipynb: permanent magnet undulators
- Undulators_HYB.ipynb: hybrid undulators (PM + iron poles)



